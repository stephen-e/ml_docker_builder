#!/usr/bin/env python3
import argparse
import subprocess
import os
import shutil


def main():
    args = parse_args()

    # determine remote repo type
    repo_is_mercurial = False
    repo_is_git = False
    print("::: testing repo...")
    try:
        subprocess.run(["hg", "identify", args.repo], check=True)
        repo_is_mercurial = True
    except subprocess.CalledProcessError:
        print("::: not Mercurcial")

    try:
        subprocess.run(["git", "ls-remote", args.repo], check=True)
        repo_is_git = True
    except subprocess.CalledProcessError:
        print("::: not Git")

    if repo_is_mercurial and repo_is_git:
        print("::: ERROR: repo seems to be both Git and Mercurial???")
        exit(-1)
    if not repo_is_mercurial and not repo_is_git:
        print("::: repo didn't seem to be Git or Mercurial - is the URL correct?")
        exit(-1)

    # remove build dir from last time
    try:
        shutil.rmtree("repo")
    except FileNotFoundError:
        pass

    # clone into ./repo
    if repo_is_mercurial:
        hg_clone(args.repo)
    else:
        git_clone(args.repo)

    # create local place for output files to go
    OUTPUT_DIR = "ml_build"
    if os.path.exists(OUTPUT_DIR):
        shutil.rmtree(OUTPUT_DIR)
    os.mkdir(OUTPUT_DIR)

    # build setup
    cpu_count = os.cpu_count()
    if not cpu_count:
        cpu_count = 1
    # ML can be fussy about which dir you are in when you make
    os.chdir("repo/platform/" + args.camera)

    make_invocation = ["make", "-j", str(cpu_count)]
    print("::: About to make, using: %s" % make_invocation)
    #subprocess.run(make_invocation, shell=True, check=True)
    subprocess.run("make -j" + str(cpu_count), shell=True, check=True)

    # get the files out to host
    shutil.copy("autoexec.bin", "../../../ml_build/autoexec.bin")


def hg_clone(repo):
    # hg is stupid and doesn't have a nice way of cloning a branch,
    # or, the hg branch model is stupid,
    # or, I don't understand hg.
    #
    # regardless, we must parse the repo URL to extract potential
    # branch name in order to get anything sensible done.

    if "/branch/" in repo:
        repo = repo.replace("/branch/", "#")
        # hg clone https://user@cloneurl/my_product#MY_BRANCH

    subprocess.run(["hg", "clone", repo, "repo"], check=True)


def git_clone(repo):
    subprocess.run(["git", "clone", repo, "repo"], check=True)
    # RUN git clone https://bitbucket.org/stephen-e/ml_200d


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("repo",
                        help="URL of Magiclantern repo to download, e.g., "
                             "https://bitbucket.org/hudson/magic-lantern/branch/unified"
                       )
    parser.add_argument("camera",
                        help="which camera to build for, e.g., 5D3.113"
                       )

    args = parser.parse_args()
    return args                    


if __name__ == "__main__":
    main()
