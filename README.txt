To use this you will first need to install Docker.
https://docs.docker.com/install/

I wrote this on Linux and have not tested on other OSes.  In theory,
it should work on Mac and Windows too.

After installing Docker, use a shell in the same directory as these files and
run the following commands:

docker build -t ml_build .
docker rm ml_build_output
docker create --name ml_build_output ml_build https://bitbucket.org/hudson/magic-lantern/branch/unified 5D3.113
docker start --attach ml_build_output
docker cp ml_build_output:/home/ml_builder/ml_build/autoexec.bin .

The "docker rm" command may give an error; this can be ignored.

If everything went well, after "docker start" is finished, autoexec.bin should exist in the same directory.

If you want to use a different build, you need to change the repo listed in the "docker create" line.
