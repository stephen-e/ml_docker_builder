# build with:
# docker build -t ml_build .
FROM debian:buster-slim
LABEL description="Magiclantern Builder"

# clean is not needed with official Debian images,
# RUN apt-get clean

# update is required since we later install packages,
# and Debian images ship with empty apt cache
RUN apt-get update

# install stuff we need to build
RUN apt-get install -qy mercurial
RUN apt-get install -qy git
RUN apt-get install -qy vim
RUN apt-get install -qy build-essential
RUN apt-get install -qy gcc-arm-none-eabi
RUN apt-get install -qy python3

# create user for building with,
# and copy in the script that runs the build
RUN useradd -ms /bin/bash ml_builder
WORKDIR /home/ml_builder
COPY ml_build_setup.py .
RUN chown ml_builder:ml_builder ml_build_setup.py
RUN chmod u+x ml_build_setup.py
USER ml_builder

# In order to allow Windows, Mac and Linux to all run
# the build in the same way, we want to avoid doing
# config via environment variables, because Windows
# is "special".  The only other way I know to pass
# in params is via CMD, which if you specify here,
# then trailing args to docker run and docker create 
# get passed to it.
ENTRYPOINT ["/home/ml_builder/ml_build_setup.py"]
CMD ["dummy_arg"]
